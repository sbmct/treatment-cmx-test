to run heatmap daemon, 

go to the "static layer 3" launchpad button, drag the "All data Heatmap" onto the map, and, inside of the virtualenv (source venv/bin/activate), 
run Jeffs 'getClientHistoryDB.py' script and put the resulting 'cmxdata.db' into the share directory of the treatment.
I used floor id - 723413320329068590. This creates data at the end of 08-10-16 starting at around 17:00:00. Otherwise the heatmap returned will be blank

cd share

python cmx_tracking/daemons/heatmap.py

you should see it print out a bunch of stuff, and the heatmap should show up
