import copy
import heatmap
from sluice.geometry import Location
from plasma import Protein


BASE_DES = [
    'sluice', 'prot-spec v1.0', 'request', 'texture',
    ['SlawedQID', [255, 61, 206, 165, 130, 14, 115, 162, 74, 144, 204, 26, 190, 22, 0, 0]],
    'CMXHeat'
]
BASE_ING = {
    'attributes': {
        'dotwidth': ['16'],
        'entity-attributes': ['active'],
        'entity-kinds': ['dummy-scanner'],
        'opacity': ['0.75'],
        'style': ['cmx_tracking/colors/red.png'],
        'texture-wait': ['10.0'],
        'window': [3600]
    },
    'bl': [0, 0],
    'tr': [0, 0],
    'px': [1907, 1080],
    'dotwidth': 16,
    'live': False,
    'name': 'image-0x7faa29287a00',
    'style': '',
    'time': 1431497952.0,
    'data': [],
}

KIND_STYLE = {
    'forklift': 'red-yellow',
    'scanner': 'indigo-blue',
    'other': 'green',
    'all': 'colors'
}


def req(store, time, window, kind):
    style = 'cmx_tracking/colors/%s.png' % KIND_STYLE[kind]
    ing = copy.deepcopy(BASE_ING)
    ing['attributes']['entity-kinds'] = ['dummy-%s' % kind]
    ing['attributes']['style'] = [style]
    ing['attributes']['window'] = [window]
    ing['time'] = float(time)
    if store == 'east':
        bl = heatmap.ESTORE_BL
        tr = heatmap.ESTORE_TR
    else:
        bl = heatmap.WSTORE_BL
        tr = heatmap.WSTORE_TR
    ing['bl'] = bl
    ing['tr'] = tr

    bll = Location(*bl)
    trl = Location(*tr)
    w = heatmap.STORE_PX[0]
    texture = heatmap.CMXHeatmapTexture(bll, trl, w)
    texture.generate(Protein(BASE_DES, ing))


def main():

    for store in ('east', 'west'):
        if store == 'west':
            mint = heatmap.WDATA_START
            maxt = heatmap.WDATA_END
        else:
            mint = heatmap.EDATA_START
            maxt = heatmap.EDATA_END
        for kind in ('forklift', 'scanner', 'all'):
            req(store, mint, 0, kind)
            for win in (3600, 43200, 86400):
                min_tw = (mint/win)*win #implied floor
                max_tw = ((maxt/win)+1)*win #floor+1

                req(store, mint, win, kind)
                for time in xrange(min_tw, max_tw, win):
                    req(store, time, win, kind)
                req(store, maxt, win, kind)

if __name__ == '__main__':
    main()

