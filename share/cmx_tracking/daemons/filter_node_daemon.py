from sluice.api import Sluice

class ClickDaemon(Sluice):
    edge_listen = True

    def __init__(self, *args, **kwargs):
        self.my_fluoroscopes = []
        super(ClickDaemon, self).__init__(*args, **kwargs)

    def handle_fluoroscope_list(self, p):
        """
        Store the list of fluoroscopes and their mapping to video sources
        """
        fluoroscopes = p.ingests().get('fluoroscopes')
        self.my_fluoroscopes = []
        for fluoro in fluoroscopes:
            if fluoro.get('name') in ('Forklift Tracking', 'Scanner Tracking'):
                self.my_fluoroscopes.append(fluoro)

    def _on_kind(self, kind):
        #Find to see if filtering or not
        for fluoro in self.my_fluoroscopes:
            test_fluoro = False
            for attr in fluoro.get('attributes', []):
                if attr.get('name') == 'enabled-kinds':
                    #a tiny bit brittle
                    if attr['contents'][0]['name'].startswith(kind[:5]):
                        #this fluoro shows the type that we like
                        test_fluoro = True
                        break
            if test_fluoro:
                for attr in fluoro.get('attributes', []):
                    if attr.get('name') == 'dynamic-filters-whitelist':
                        if not bool(attr['contents'][0]['selected']):
                            #already turned 'All' off
                            return True
                        else:
                            return False
        return False

    def handle_poked_it(self, msg):
        node = msg.ingests()
        if node.get('kind') in ('forklift', 'scanner', 'forklift-path', 'scanner-path'):
            mac_address = node.get('attrs', {}).get('mac_address')
            mac_addresses = node.get('mac_addresses') #non-sluice message support multiple macs
            if mac_address or mac_addresses:
                already_filtering = self._on_kind(node['kind'])

                if mac_addresses:
                    if not mac_address:
                        mac_address = 'Mac Filter List'
                    mac_filter_list = ['"%s"'% ma for ma in mac_addresses]
                else:
                    mac_filter_list = ['"' + mac_address + '"']

                for fluoro in self.my_fluoroscopes:
                    for attr in fluoro.get('attributes', []):
                        if attr.get('name') == 'dynamic-filters-whitelist':
                            #change this one, and break for this fluoro
                            all_on = 1 if already_filtering else 0
                            mac_on = 0 if already_filtering else 1
                            attr['contents'] = [
                                {
                                    'name': 'true',
                                    'display-text': 'All',
                                    'selected': all_on,
                                },
                                {
                                    'name': '([%s]).indexOf(observation(n, "mac_address", t)) >= 0' % ", ".join(mac_filter_list),
                                    'display-text': '%s' % mac_address,
                                    'selected': mac_on,
                                }
                            ]
                            self.configure_fluoroscope(fluoro)
                            #don't check any more attrs (still in fluoro for loop)
                            break

def main():
    s = ClickDaemon()
    s.run()
    s.quit()

if '__main__' == __name__:
    main()
