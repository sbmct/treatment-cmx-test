import os.path
import math
from cStringIO import StringIO
import numpy as np
from matplotlib.colors import LinearSegmentedColormap
import matplotlib.pyplot as plt
import scipy.ndimage as ndi

from sluice.api import Sluice
from sluice.geometry import Location
from sluice.mercator import Mercator
from sluice.texture import Texture, TextureFluoro

import traceback
import urllib2

from colors import get_colormap

import sqlite3
import sys
sys.path.insert(0, os.path.join(os.path.dirname(__file__), '..', 'data'))
import cmx_locs
from datetime import datetime

APP = 'sluice'
PROTO = 'prot-spec v1.0'

CACHE_DIR = os.path.join(os.path.dirname(__file__), 'cache')

WSTORE_BL = [51.330689887182764, 5.9153883628155279]
WSTORE_TR = [51.33251876794683, 5.9204808816498344]
ESTORE_BL = [51.658706358768924, -0.025927875862168381]
ESTORE_TR = [51.660737391309041, -0.020231608468395113]
STORE_PX = [1878, 1080]

WSTORE_CENT = [0.5*(WSTORE_BL[0] + WSTORE_TR[0]), 0.5*(WSTORE_BL[1] + WSTORE_TR[1])]
ESTORE_CENT = [0.5*(ESTORE_BL[0] + ESTORE_TR[0]), 0.5*(ESTORE_BL[1] + ESTORE_TR[1])]

WDATA_START = 1470290397
WDATA_END   = 1470874135
EDATA_START = 1430949605
EDATA_END   = 1431497952
import sqlite3
import math
from datetime import datetime

def _sql_time(epoch_time):
    dt_obj = datetime.fromtimestamp(float(epoch_time))
    return dt_obj.strftime('%Y-%m-%d %H:%M:%S.%f')

def _get_sql_data(store, time, window, kind):
    db_file='cmxdata.db'
    conn = sqlite3.connect(db_file)
    conn.row_factory = sqlite3.Row
    with conn:
        cur = conn.cursor()

        params = []
        query = ('SELECT x, y from cmx_data')
        #simple_query = query
        #test_time_start = _sql_time(1470290397)
        #test_time_end = _sql_time(1470874135)
        print 'from',_sql_time(time-window),' to ',_sql_time(time)
        if window > 0:
            if not params:
                query += ' where '
            else:
                query += ' and '
            query += 'last_located_time >= ? and last_located_time < ?'
            params = list(params) + [_sql_time(time-window), _sql_time(time)]
            #params = list(params) + [test_time_start, test_time_end]
        cur.execute(query+';', params)
        #cur.execute('SELECT x, y from cmx_data WHERE last_located_time >= ? and last_located_time < ?',('2016-08-03 22:59:57.000000', '2016-08-10 17:08:55.000000'))
        geo_locs = []
        foot_locs = []
        loc_to_ll = cmx_locs.west_loc if store == 'west' else cmx_locs.east_loc
        for data in cur.fetchall():
            geo_locs.append(loc_to_ll((data['x'], data['y'])))
            foot_locs.append((data['x'],data['y']))
        return geo_locs,foot_locs
def _alexs_get_sql_data(store,time,window,kind):
    now = datetime.now().strftime('%s')
    db_file = 'cmxdata.db'
    conn = sqlite3.connect(db_file)
    conn.row_factory = sqlite3.Row
    window = 36000000
    pos_or_neg = 1 # or neg 1
    start = _sql_time(int(now) - window)
    end = _sql_time(now)
    with conn:
        cur = conn.cursor()
        cur.execute('SELECT x, y from cmx_data WHERE last_located_time >= ? and last_located_time < ?',('2016-08-03 22:59:57.514000','2016-08-10 17:08:55.523000'))
        locs = []
        for data in cur.fetchall():
            locs.append((data['x'],data['y']))
    return locs

def clean_generate(store, attributes, time, bounds):
    """
    This generates the texture.  I would use conduce_texture, but it has
    some issues on ubuntu (at least it did in the aces lab: CORE-66)
    """
    w = int(bounds.w)
    h = int(bounds.h)
    grid = np.zeros((h+1, w+1))
    #Get sql data
    window = int(attributes['window'][0])
    #round the time up
    if window > 0:
        ctime = math.ceil(time/window) * window
    else:
        ctime = 0
    kind = attributes['entity-kinds'][0]
    style = attributes.get('style', ['red'])[0]

    geo_locs,foot_locs = _get_sql_data(store, ctime, window, kind)
    print len(geo_locs),'there is date in the geo_locs'
    count = 0
    x_feet = [item[0] for item in foot_locs]
    y_feet = [item[1] for item in foot_locs]
    if len(x_feet) > 0:
        x_middle_feet = sum(x_feet)/len(x_feet)
        y_middle_feet = sum(y_feet)/len(y_feet)
        grid = np.zeros((max(x_feet)-min(x_feet),max(y_feet)-min(y_feet)))
    else:
        x_middle_feet = 0
        y_middle_feet = 0
        grid = np.zeros((w,h))
    for item in foot_locs:
        #x,y = bounds.project(Location(item[0],item[1]))
        #print x,y
        #try:
        #    grid[int(y)][int(x)] += 1
        #    count += 1
        #    print "was in the image"
        #except IndexError:
        #    print "does not belong in the image"
        #    pass

        x_pos = item[0]-x_middle_feet
        y_pos = item[1]-y_middle_feet
        grid[int(x_pos)][int(y_pos)]+=1

    dotwidth = 64
    r = math.sqrt(dotwidth)
    grid = ndi.gaussian_filter(grid, (r, r))
    style =  attributes.get('style')[0]

    new_fig = plt.figure(None, figsize=(w/100.0, h/100.0), dpi=100.0)
    cmap = LinearSegmentedColormap('hm_%s' % style, get_colormap(style), 256)
    plt.imshow(grid, cmap=cmap, origin='lower')

    #turn off axes, ticks, etc.
    new_fig.tight_layout(pad=0)
    a = new_fig.gca()
    a.set_frame_on(False)
    a.set_xticks([])
    a.set_yticks([])
    plt.axis('off')
    plt.xlim(0, w)
    plt.ylim(h, 0)

    #Generate PNG data for the plot
    png = StringIO()
    plt.savefig(png, format='png', bbox_inches='tight', transparent=True, pad_inches=0)
    plt.close('all')
    png.seek(0)
    return png, w, h

def _get_store(ingests):
    center = [0.5*(ingests['bl'][0]+ingests['tr'][0]), 0.5*(ingests['bl'][1]+ingests['tr'][1])]
    dw_lat = center[0] - WSTORE_CENT[0]
    dw_lon = center[1] - WSTORE_CENT[1]
    de_lat = center[0] - ESTORE_CENT[0]
    de_lon = center[1] - ESTORE_CENT[1]
    dw = dw_lat*dw_lat+dw_lon*dw_lon
    de = de_lat*de_lat+de_lon*de_lon
    if de < dw:
        store = 'east'
    else:
        store = 'west'
    return store



class CMXHeatmapTexture(Texture):

    def generate(self, p):
        ingests = p.ingests()
        store = _get_store(ingests)
        if store == 'east':
            store_bl = ESTORE_BL
            store_tr = ESTORE_TR
        else:
            store_bl = WSTORE_BL
            store_tr = WSTORE_TR
        bl = Location(*store_bl)
        tr = Location(*store_tr)
        w = STORE_PX[0]
        bounds = Mercator(bl, tr, w)
        attributes = ingests['attributes']
        time = ingests['time']
        #Get kind, time, window, color for caching
        #Get sql data
        window = int(attributes['window'][0])
        if window > 0:
            ctime = int(math.ceil(float(time)/window) * window)
            if store =='east':
                ctime = max(ctime, EDATA_START)
                ctime = min(ctime, EDATA_END)
            else:
                ctime = max(ctime, WDATA_START)
                ctime = min(ctime, WDATA_END)
        else:
            ctime = 0
        kind = attributes['entity-kinds'][0]
        style = attributes.get('style', ['red'])[0]
        dotwidth = int(float(attributes.get('dotwidth', [32])[0]))

        cache_name = '%s|%s|%s|%s|%s|%s' % (kind, store, ctime, window, dotwidth, urllib2.quote(style, ''))
        cache_path = os.path.join(CACHE_DIR, cache_name)

        if os.path.exists(cache_path):
            png = open(cache_path, 'r')
            print "using existing image at",cache_path
        else:
            print "cache not found",time,window,'and we are regenerating'
            png, w, h = clean_generate(store, attributes, time, bounds)
            fh = open(cache_path, 'wb')
            fh.write(png.read())
        self._png = png

    def png(self):
        self._png.seek(0)
        return self._png


class CMXHeatmapFluoro(TextureFluoro):
    name = 'CMXHeat'
    texture_class = CMXHeatmapTexture

    def handle_texture_request(self, p):
        """
        hacking around this to make life easier - using constant bl/tr bounds
        """
        try:
            ingests = p.ingests()
            store = _get_store(ingests)
            if store == 'east':
                store_bl = ESTORE_BL
                store_tr = ESTORE_TR
            else:
                store_bl = WSTORE_BL
                store_tr = WSTORE_TR
            bl = Location(*store_bl)
            tr = Location(*store_tr)
            w, h = STORE_PX
            if w <= 0 or h <= 0:
                # bad dimensions, ignore
                return False
            texture = self.texture_class(bl, tr, w)
            if 'disabled' in p.ingests()['style'].split(','):
                texture.empty()
            else:
                texture.generate(p)
            data = texture.png()
            if True:
            #if self.texture_debug:
                outfn = '/tmp/%s.png' % self.name
                print 'saving texture to %s' % outfn
                open(outfn, 'w').write(data.read())
            data.seek(0)
            self.send_texture(p, data, px=[int(texture.w), int(texture.h)],
                                bl=store_bl, tr=store_tr)
            return True
        except KeyboardInterrupt:
            raise
        except Exception as e:
            traceback.print_exc()
            return False

def main():
    s = CMXHeatmapFluoro()
    s.startup()
    s.run()
    s.quit()

def request_current_time():
    """
    Sends a request to sluice for its notion of the current time.
    """
    d = [APP, PROTO, 'request', 'time']
    i = {}
    self.send_to_pool(Protein(d, i), EDGE_REQUEST_POOL)

if '__main__' == __name__:
    main()

