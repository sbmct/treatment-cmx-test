import os
import requests

XML_DATA_DIR = os.path.join(os.path.dirname(__file__), 'saved_cmx')

auth=('user', 'pass')
url = 'https://64.103.39.155/api/contextaware/v1/location/history/clients'

i = 1
while True:
    print "Getting: %s" % i
    r = requests.get('%s?sortBy=lastLocatedTime&locatedAfterTime=2015-02-05T12:33:48.647-0500&page=%s' % (url, i),
                     auth=auth, verify=False)
    if r.status_code != 200:
        break

    fh = open(os.path.join(XML_DATA_DIR, 'saved_%03d.xml' % i), 'w')
    fh.write(r.text)
    fh.close()
    i += 1

