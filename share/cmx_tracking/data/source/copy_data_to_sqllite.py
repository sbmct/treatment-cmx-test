import os
import xml.etree.cElementTree as ET
from datetime import datetime, timedelta

import sqlite3


#CMX is XML
XML_DATA_DIR = os.path.join(os.path.dirname(__file__), 'saved_cmx')
OUTPUT_FILE = os.path.join(os.path.dirname(__file__), 'saved_cmx_data_xml.db')


#I did a 2 pass method.  First pass copied everything, then I
#got the top 25 macs, and 2nd pass filtered to only copy the
#top 25 macs
CLEAN_MACS = (
    '00:1d:a2:80:3c:1e',
    '10:a5:d0:8f:9a:72',
    'dc:71:44:bd:1a:1e',
    '24:77:03:a3:af:ac',
    '14:2d:27:50:03:f5',
    '64:20:0c:34:7d:c9',
    '3c:a9:f4:6f:9e:08',
    '54:35:30:cc:03:b9',
    '24:77:03:aa:70:b4',
    '5c:0a:5b:32:57:fb',
    '74:e1:b6:90:81:1b',
    '3c:a9:f4:33:b7:30',
    '28:b2:bd:cf:99:6e',
    '48:51:b7:a4:de:4f',
    '1c:3e:84:1c:a1:11',
    'dc:71:44:91:4c:6d',
    '28:b2:bd:d0:2f:e1',
    '98:0d:2e:6f:ef:fa',
    '54:35:30:cc:05:a9',
    '1c:1a:c0:60:86:3e',
    'f0:db:f8:68:56:51',
    '24:77:03:dd:ed:a0',
    '64:89:9a:4f:64:04',
    'e8:2a:ea:7b:29:c9',
    '44:4c:0c:c2:01:c4',
)


def read_cmx_data(fname):
    tree = ET.parse(os.path.join(XML_DATA_DIR, fname))
    root = tree.getroot()

    all_children = []
    cdict = {}

    for child in root:
        if child.tag == "WirelessClientLocation":
            child_id = child.get('macAddress')
            if child_id and child_id in CLEAN_MACS:
                child_data = child.attrib
                child_data.pop('macAddress', None)
                for info in child:
                    if info.tag == 'MapInfo':
                        child_data['floorRefId'] = info.get('floorRefId')
                        child_data['floorImage'] = info.find('Image').get('imageName')
                    elif info.tag == 'MapCoordinate':
                        #TODO: Consider parsing out more info here
                        child_data['loc'] = [info.get('x'), info.get('y')]
                    elif info.tag == 'Statistics':
                        #appears as though lastLocatedTime is the timestamp
                        for tag in ('lastLocatedTime', 'firstLocatedTime'):
                            child_data[tag] = info.get(tag)
                yield (child_id, child_data)

def clean_time(dt_str):
    #put time in a standard format with no timezone
    dt_str_no_tz = dt_str[:-5]
    tz_offset_hr = float(dt_str[-5:-2])
    dt_obj = datetime.strptime(dt_str_no_tz, '%Y-%m-%dT%H:%M:%S.%f')
    dt_obj = dt_obj - timedelta(hours=tz_offset_hr)
    #return like 2009-01-02 05:09:04.000001
    return dt_obj.strftime('%Y-%m-%d %H:%M:%S.%f')

def main():
    if os.path.exists(OUTPUT_FILE):
        return
    conn = sqlite3.connect(OUTPUT_FILE)
    cur = conn.cursor()
    cur.execute('''CREATE TABLE cmx_data
             (mac_address text, x real, y real, last_located_time text, first_located_time text)''')

    file_list = [fname for fname in os.listdir(XML_DATA_DIR) if fname.startswith('saved') and fname.endswith('.xml')]
    file_list.sort()
    for filename in file_list:
        for (child_id, child_data) in read_cmx_data(filename):
            clean_last_loc_time = clean_time(child_data['lastLocatedTime'])
            clean_first_loc_time = clean_time(child_data['firstLocatedTime'])
            cur.execute('INSERT INTO cmx_data VALUES (?, ?, ?, ?, ?)',
                        (child_id, float(child_data['loc'][0]), float(child_data['loc'][1]),
                         clean_last_loc_time, clean_first_loc_time
                        ))

    # Save the changes
    conn.commit()

if __name__ == '__main__':
    main()


