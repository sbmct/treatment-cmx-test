import os
import csv
from datetime import datetime, timedelta

import sqlite3


def read_cmx_data(fname):
    with open(fname) as csvfile:
        reader = csv.DictReader(csvfile)
        for child in reader:
            child_id = child.get('macAddress')
            if child_id:
                child_data = {
                    'lastLocatedTime': child['lastLocatedTime'],
                    'firstLocatedTime': child['firstLocatedTime'],
                    'loc': [child['x'], child['y']]
                }
                yield (child_id, child_data)

def copy_csv_to_sqlite(csv_file, output_file):
    conn = sqlite3.connect(output_file)
    cur = conn.cursor()
    cur.execute('''CREATE TABLE cmx_data
             (mac_address text, x real, y real, last_located_time text, first_located_time text)''')

    for (child_id, child_data) in read_cmx_data(csv_file):
        clean_last_loc_time = child_data['lastLocatedTime'] + '.0'
        clean_first_loc_time = child_data['firstLocatedTime'] + '.0'
        cur.execute('INSERT INTO cmx_data VALUES (?, ?, ?, ?, ?)',
                    (child_id, float(child_data['loc'][0]), float(child_data['loc'][1]),
                     clean_last_loc_time, clean_first_loc_time
                    ))

    # Save the changes
    conn.commit()

def main():
    data_dir = os.path.dirname(__file__)
    #read cmx convereted to csv
    csv_data = os.path.join(data_dir, 'real_warehouse.csv')
    output_file = os.path.join(data_dir, 'saved_cmx_data.db')
    if not os.path.exists(output_file):
        copy_csv_to_sqlite(csv_data, output_file)
    csv_data = os.path.join(data_dir, 'curated_cmx_data.csv')
    output_file = os.path.join(data_dir, 'curated_cmx_data.db')
    if not os.path.exists(output_file):
        copy_csv_to_sqlite(csv_data, output_file)


if __name__ == '__main__':
    main()


