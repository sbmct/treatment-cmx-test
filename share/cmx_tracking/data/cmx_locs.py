import sqlite3
import os
from datetime import datetime
from sluice.api import Sluice
import copy
import collections
import time

DB_FILE = os.path.join(os.path.dirname(__file__), 'source', 'saved_cmx_data.db')
CMX_DB_FILE = os.path.join(os.path.dirname(__file__), 'source', 'saved_cmx_data_xml.db')
CURATED_DB_FILE = os.path.join(os.path.dirname(__file__), 'source', 'curated_cmx_data.db')


def top_macs():
    """
    Call to get the most popular mac addresses.
    This is a first cut at figuring out what is a forklift
    """
    conn = sqlite3.connect(DB_FILE)
    conn.row_factory = sqlite3.Row

    with conn:
        cur = conn.cursor()
        #adding 'limit' limits before count(*)
        cur.execute('SELECT mac_address, count(*) as num from cmx_data group by mac_address order by num desc;')

        count = 1
        for data in cur.fetchall():
            print count, data['mac_address'], data['num']
            count += 1
            if count > 20:
                break


#
# West: Got the first 10 mac addresses and put them as forklifts
# everything else is scanners
# East: Pre-filtered the top 50 mac addresses.  The top 10 are
#also forklifts
#
FORKLIFTS = (
    '00:23:68:61:92:95',

    '00:23:68:ce:26:80',
    '00:23:68:ce:2a:50',
    '00:23:68:60:0d:de',
    '00:23:68:60:10:24',
    '00:23:68:60:0d:80',
    '00:23:68:b1:e8:e9',
    '00:23:68:ce:2a:fd',
    '00:23:68:ce:25:6d',
    '00:23:68:60:11:9a',

    '00:1d:a2:80:3c:1e',
    '10:a5:d0:8f:9a:72',
    'dc:71:44:bd:1a:1e',
    '24:77:03:a3:af:ac',
    '14:2d:27:50:03:f5',
    '64:20:0c:34:7d:c9',
    '3c:a9:f4:6f:9e:08',
    '54:35:30:cc:03:b9',
    '24:77:03:aa:70:b4',
    '5c:0a:5b:32:57:fb',
)


LAT_MIN = 51.3311795
LAT_MAX = 51.3321245
LON_MIN = 5.917299
LON_MAX = 5.918559
DLAT = LAT_MAX - LAT_MIN
DLON = LON_MAX - LON_MIN
def west_loc(loc):
    x = loc[0] - 20
    y = loc[1]
    xn = (x / 560.0)
    yn = (1 - (y / 655.0))
    dlat_pt = yn * DLAT
    dlon_pt = xn * DLON
    return [LAT_MIN + dlat_pt, LON_MIN + dlon_pt, 0.0]

ELAT_MIN = 51.6592398
ELAT_MAX = 51.6601848
ELON_MIN = -0.0238053
ELON_MAX = -0.0225453
EDLAT = ELAT_MAX - ELAT_MIN
EDLON = ELON_MAX - ELON_MIN
def east_loc(loc):
    #X is (-4, 256) & y is (-4, 173)
    x = loc[0] + 5
    y = loc[1] + 5
    dlat_pt = (x / 261.0) * EDLAT
    dlon_pt = (y / 178.0) * EDLON
    #a little fudge since icon centers go to the edge,
    #and they spill over the outside
    dlat_pt = (dlat_pt * 0.65) + (EDLAT * 0.25)
    dlon_pt = (dlon_pt * 0.65) + (EDLON * 0.05)
    return [ELAT_MIN + dlat_pt, ELON_MIN + dlon_pt, 0.0]


EPOCH_TIME = datetime(1970, 1, 1)
def float_time(dt_str):
    dt_obj = datetime.strptime(dt_str, '%Y-%m-%d %H:%M:%S.%f')
    return (dt_obj - EPOCH_TIME).total_seconds()

def make_records(cur, store, mac_addresses, kind, path=False, expire=None):
    for mac_address in mac_addresses:
        loc_fun = east_loc if store == 'east' else west_loc
        #TODO: Might be faster to group by mac_address and track
        #when mac address changes instead of making multiple queries
        #adding 'limit' limits before count(*)
        cur.execute('SELECT * from cmx_data where mac_address = ?',
                    (mac_address,))

        path_pts = collections.deque()
        timestamp = None
        for data in cur.fetchall():
            attrs = {
                'mac_address': data['mac_address'],
                'first_located_time': data['first_located_time'],
            }
            if expire:
                attrs['active'] = 'true'
            ll_loc = loc_fun([data['x'], data['y']])
            timestamp = float_time(data['last_located_time'])

            record = {
                'id': data['mac_address'],
                'kind': kind,
                'timestamp': timestamp,
                'loc': ll_loc,
                'attrs': attrs
            }
            yield record

            if path:
                path_pts.append(ll_loc)
                if len(path_pts) > 3:
                    path_pts.popleft()
                record = {
                    'id': '%s-path' % data['mac_address'],
                    'kind': '%s-path' % kind,
                    'timestamp': timestamp,
                    'path': copy.deepcopy(path_pts),
                    'attrs': attrs
                }
                yield record


def expire_after(cur, mac_addresses, expire_delta):
    for mac_address in mac_addresses:
        #adding 'limit' limits before count(*)
        cur.execute('SELECT * from cmx_data where mac_address = ? ORDER BY last_located_time DESC LIMIT 1',
                    (mac_address,))
        for data in cur.fetchall():
            timestamp = float_time(data['last_located_time']) + expire_delta
            obs = {
                'id': data['mac_address'],
                'timestamp': timestamp,
                'attrs': {
                    'active': 'false'
                }
            }
            yield obs

def list_other_macs(cur):
    #Boo sqllite
    known_macs = list(FORKLIFTS)
    cur.execute('SELECT distinct(mac_address) from cmx_data where mac_address not in (%s);'%
                ','.join(['?'] * len(known_macs)), known_macs)

    other_macs = []
    for data in cur.fetchall():
        other_macs.append(data['mac_address'])
    return other_macs


def main():
    import argparse
    ap = argparse.ArgumentParser()
    ap.add_argument('-f', '--file', default=None)
    ap.add_argument('-t', '--type', default=None, choices=('forklifts', 'scanners'))
    ap.add_argument('-s', '--store', default='west', choices=('east', 'west'))
    args = ap.parse_args()
    output = args.file
    if output is not None:
        output = open(output, 'w')
    s = Sluice(output=output)
    store = args.store

    if store == 'west':
        db_file = CURATED_DB_FILE
    else:
        db_file = CMX_DB_FILE
    conn = sqlite3.connect(db_file)
    conn.row_factory = sqlite3.Row
    with conn:
        cur = conn.cursor()
        start = time.time()

        if args.type in (None, 'forklifts'):
            s.inject_topology(make_records(cur, store, FORKLIFTS, 'forklift', path=True))
        if args.type in (None, 'scanners'):
            other_macs = list_other_macs(cur)
            s.inject_topology(make_records(cur, store, other_macs, 'scanner', path=True))

        end = time.time()
        print end - start

if __name__ == '__main__':
    main()

