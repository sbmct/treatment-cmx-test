from sluice import Sluice
from sluice.types import v3float64
import time
import csv
import random
import json
from requests.auth import HTTPBasicAuth
import requests
import datetime,calendar
WSTORE_BL = [51.330689887182764, 5.9153883628155279]
WSTORE_TR = [51.33251876794683, 5.9204808816498344]
ESTORE_BL = [51.658706358768924, -0.025927875862168381]
ESTORE_TR = [51.660737391309041, -0.020231608468395113]
def inject(lat,lon,client_id):
    data = []
    for i in range(1):
        record = {
            'id': str(client_id),
            'kind': 'client',
            'loc': v3float64(float(lat),float(lon), 0.0),
            'attrs': {'value':'0'}
        }
        data.append(record)
    return data

def update(A):
    data = []
    for i in range(len(A)):
        rand = A[random.randint(0,2)]
        value = random.randint(1,3)     
        record = {
            'id': rand,
            'timestamp':calendar.timegm(datetime.datetime(2016, 12, 15).utctimetuple()),
            'attrs': {'value':str(random.randint(0,2))}
        }
        data.append(record)
    print data
    return data

def convert(x,y,building):
    lat = 0
    lon = 0
    if building == 'WSTORE':
        lat = WSTORE_BL[0] + 0.000363*y
        lon = WSTORE_BL[1] + 0.000363*x
    elif building == 'ESTORE':
        lat = ESTORE_BL[0] + 0.00063*y
        lon = ESTORE_BL[1] + 0.00063*x 
    else: 
        print 'building didnt match'
    return lat,lon

def query_cmx(s):
    headers = {'Accept': 'application/json'}
    username = 'learning'
    password = 'learning'
    restURL = 'https://msesandbox.cisco.com:8081/api/location/v2/clients'
    try:
        r = requests.get(
               url = restURL,
               auth = HTTPBasicAuth(username,password),
               headers=headers,
               verify=False)
        if r.status_code == 200:
            parsed = json.loads(r.content)
            for item in parsed:
                x = item['mapCoordinate']['x']
                y = item['mapCoordinate']['y']
                building = bool(random.getrandbits(1))
                if building:
                    building = 'WSTORE'
                else:
                    building = 'ESTORE'
                lat,lon = convert(x,y,building)
                client_id = item['macAddress']
                print lat,lon,client_id,building
                s.inject_topology(inject(lat,lon,client_id))
        else:
            print r
    except requests.exceptions.RequestException as e:
        print e

def main():
    s = Sluice()
    #s.inject_topology(inject())
    while True:
        #s.update_observations(update(A))
        query_cmx(s)
        time.sleep(1)

if __name__=="__main__":
    A = ['A','B','C'] #3 sample ids 
    main()
